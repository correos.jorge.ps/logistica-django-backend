from django.conf.urls import url
from . import classviews

urlpatterns = [
    url(r'^$', classviews.StatsView.as_view(), name='stats_view'),
]

from django.utils.translation import gettext_lazy as _
from django.contrib.gis.db import models
from logistica.models.base import BaseModel


class Request(BaseModel):
    STATUS_AWAITING = 0
    STATUS_SENT = 1
    STATUS_RECEIVED = 2
    STATUS_SCHEDULED = 3
    STATUS_CANCELLED = 4

    STATUS_CHOICES = (
        (STATUS_AWAITING, 'Esperando'),
        (STATUS_SENT, 'Enviado'),
        (STATUS_RECEIVED, 'Recibido'),
        (STATUS_SCHEDULED, 'Programado'),
        (STATUS_CANCELLED, 'Cancelado'),
    )
    status = models.IntegerField(
        default=STATUS_AWAITING,
        choices=STATUS_CHOICES,
        verbose_name='Estado',
    )
    consumer = models.ForeignKey(
        'Person',
        null=False,
        related_name='requests',
        blank=False,
        on_delete=models.PROTECT,
    )
    resource = models.ForeignKey(
        'Resource',
        null=False,
        related_name='requests',
        blank=False,
        on_delete=models.PROTECT,
        verbose_name='Recurso',
    )
    quantity = models.IntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name='Cantidad',
    )
    done = models.BooleanField(
        default=False,
        verbose_name="Necesidad satisfecha",
    )

    class Meta:
        verbose_name_plural = _('requests')

    def __str__(self):
        try:
            consumer_str = self.consumer.name
        except AttributeError:
            consumer_str = ''
        done_str = ''
        if self.done:
            done_str = ' | Satisfecha | '
        return f"{consumer_str} -> {self.quantity} -> {done_str} {self.resource.name}"

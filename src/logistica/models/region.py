import uuid as uuid_lib
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.contrib.gis.db import models
from logistica.models.base import BaseModel


# Not really used model. Implemented just to cross data between
# shape datasets with 'crossgeospatial' command
class Municipios(models.Model):
    objectid = models.BigIntegerField()
    inspireid = models.CharField(max_length=80)
    natcode = models.CharField(max_length=80)
    nameunit = models.CharField(max_length=81)
    codnut1 = models.CharField(max_length=80)
    codnut2 = models.CharField(max_length=80)
    codnut3 = models.CharField(max_length=80)
    codigoine = models.CharField(max_length=80)
    shape_are = models.FloatField()
    shape_len = models.FloatField()
    geom = models.MultiPolygonField(srid=4326)

    # Auto-generated `LayerMapping` dictionary for Municipios model
    municipios_mapping = {
        'objectid': 'OBJECTID',
        'inspireid': 'INSPIREID',
        'natcode': 'NATCODE',
        'nameunit': 'NAMEUNIT',
        'codnut1': 'CODNUT1',
        'codnut2': 'CODNUT2',
        'codnut3': 'CODNUT3',
        'codigoine': 'CODIGOINE',
        'shape_are': 'Shape__Are',
        'shape_len': 'Shape__Len',
        'geom': 'MULTIPOLYGON',
    }


class Region(BaseModel):
    PERM_TRUE = True
    PERM_FALSE = False
    PERM_INHERIT = None

    PERM_CHOICES = (
        (PERM_TRUE, 'Permitir'),
        (PERM_FALSE, 'Denegar'),
        (PERM_INHERIT, 'Heredar configuración del padre'),
    )

    level = models.PositiveIntegerField(
        null=False,
        blank=False,
        default=1,
        validators=[MinValueValidator(1)],
        verbose_name="Nivel de la región",
        help_text=
        "Indica el nivel de la región. El nivel más bajo (1) identifica un código postal. Un nivel superior no podrá tener código postal."  # pylint: disable=line-too-long
    )

    parent = models.ForeignKey(
        'Region',
        default=None,
        null=True,
        related_name='children',
        blank=True,
        on_delete=models.PROTECT,
    )
    default_person_allow_set_send_status = models.BooleanField(
        default=PERM_INHERIT,
        null=True,
        blank=True,
        choices=PERM_CHOICES,
        verbose_name="Permiso de estado enviado de shipping por defecto",
        help_text=
        "Permitir por defecto setear estado enviado (de shipping) por parte del dueño del inventario. Las regiones hijas heredarán este permisos si en ellas se ha configurado 'heredar'",  # pylint: disable=line-too-long
    )

    default_person_allow_set_received_status = models.BooleanField(
        default=PERM_INHERIT,
        null=True,
        blank=True,
        choices=PERM_CHOICES,
        verbose_name="Permiso de estado recibido de shipping por defecto",
        help_text=
        "Permitir por defecto setear estado recibido (de shipping) por parte del receptor de un pedido. Las regiones hijas heredarán este permisos si en ellas se ha configurado 'heredar'",  # pylint: disable=line-too-long
    )

    name = models.CharField(
        max_length=250,
        db_index=True,
        null=True,
        blank=True,
        verbose_name='Región',
    )

    # `LayerMapping` dictionary for Region model
    region_mapping = {
        'name': 'NAME',
        'uuid': 'UUID',
        'postal_code': 'POSTAL_COD',
        'ine_code': 'INE_CODE',
        'geom': 'MULTIPOLYGON',
    }

    # Shape fields
    # uuid field prevents duplicity
    uuid = models.CharField(
        db_index=True,
        max_length=36,
        unique=True,
        default=uuid_lib.uuid4,
        editable=False,
        help_text='UUID',
        null=False,
        blank=False,
    )
    postal_code = models.CharField(
        max_length=80,
        null=True,
        blank=True,
        db_index=True,
    )
    ine_code = models.IntegerField(
        null=True,
        blank=True,
        db_index=True,
    )
    geom = models.MultiPolygonField(srid=4326, null=True, blank=True)

    class Meta:
        verbose_name_plural = _('regions')

    def clean(self):
        # Do normal clean
        super().clean()
        if self.level > 1 and self.postal_code:
            raise ValidationError({
                'level':
                _('Level greater than 1 cannot have postal code.'),
                'postal_code':
                _('Level greater than 1 cannot have postal code.'),
            })

    def __str__(self):
        return f"pk:{str(self.pk)} level:{self.level} PC:{str(self.postal_code)} - {str(self.name)}"

import uuid as uuid_lib
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.contrib.gis.db import models
from logistica.models.base import BaseModel


class Shipping(BaseModel):
    STATUS_AWAITING = 0
    # El transportista ha recogido el pedido
    STATUS_SENT = 1
    STATUS_RECEIVED = 2
    STATUS_SCHEDULED = 3
    STATUS_CANCELLED = 4
    STATUS_IN_TRANSIT = 5

    STATUS_CHOICES = (
        (STATUS_AWAITING, 'Esperando'),
        (STATUS_SENT, 'Enviado'),
        (STATUS_RECEIVED, 'Recibido'),
        (STATUS_SCHEDULED, 'Programado'),
        (STATUS_CANCELLED, 'Cancelado'),
        (STATUS_IN_TRANSIT, 'En tránsito'),
    )

    uuid = models.UUIDField(
        db_index=True,
        unique=True,
        default=uuid_lib.uuid4,
        editable=False,
        help_text='Nº único de envío',
    )
    status = models.IntegerField(
        default=STATUS_AWAITING,
        choices=STATUS_CHOICES,
        verbose_name='Estado',
        help_text=
        'Estado del envío. Si se marca como cancelado, el stock disponible del inventario será restaurado.'
    )
    carrier = models.ForeignKey(
        'Person',
        null=True,
        related_name='shipping',
        blank=True,
        on_delete=models.PROTECT,
        verbose_name='Transportista',
    )
    carrier_track_num = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Track num del transportista',
    )
    carrier_label_base64 = models.TextField(
        null=True,
        blank=True,
        verbose_name='Etiqueta transportista (en base64)',
    )
    carrier_label_base64_format = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        verbose_name='formato de etiqueta, png, pdf, etc',
    )
    # Pedido o necesidad que cubre este envío
    request = models.ForeignKey(
        'Request',
        null=True,
        related_name='shippings',
        blank=True,
        on_delete=models.PROTECT,
        verbose_name='Pedido',
    )
    # Inventario origen donde se recoge el material
    inventory = models.ForeignKey(
        'Inventory',
        null=True,
        related_name='shippings',
        blank=True,
        default=None,
        on_delete=models.PROTECT,
    )
    # Inventario destino donde se lleva el material
    move = models.ForeignKey(
        'Inventory',
        null=True,
        related_name='shippings_in',
        blank=True,
        default=None,
        on_delete=models.PROTECT,
    )
    quantity = models.IntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name='Cantidad',
        help_text=
        'La cantidad aquí reflejada será restada del inventario seleccionado')

    class Meta:
        verbose_name_plural = _('shippings')

    def clean(self):
        # Evitamos movimientos entre inventarios de distinto tipo
        super().clean()
        try:
            if self.move and self.move.resource != self.inventory.resource:
                raise ValidationError({
                    'move':
                    _('Resource of inventories mismatch.'),
                    'inventory':
                    _('Resource of inventories mismatch.'),
                })
        except Shipping.move.RelatedObjectDoesNotExist:
            pass

        except Shipping.inventory.RelatedObjectDoesNotExist:
            pass

    def __str__(self):
        if self.move:
            return f"{self.inventory} -> {self.move}"
        request_str = str(self.request)
        try:
            consumer_str = self.request.consumer.name
        except AttributeError:
            consumer_str = ''
        return f"{request_str} -> {consumer_str}"


class AutoMovementConfiguration(BaseModel):
    source_region = models.ForeignKey(
        'Region',
        null=False,
        related_name='auto_movements',
        blank=False,
        on_delete=models.CASCADE,
        verbose_name='Región',
        help_text='Cuando se cree un inventario en esta región',
    )

    resource = models.ForeignKey(
        'Resource',
        null=False,
        related_name='auto_movements',
        blank=False,
        on_delete=models.PROTECT,
        verbose_name='Recurso',
        help_text='..de este recurso',
    )

    target_inventory = models.ForeignKey(
        'Inventory',
        null=False,
        related_name='auto_movements',
        blank=False,
        default=None,
        on_delete=models.CASCADE,
        verbose_name="Inventario destino",
        help_text=
        '...moverlo a este otro inventario (normalmente el de un almacén)',
    )

    carrier = models.ForeignKey(
        'Person',
        null=True,
        related_name='auto_movements',
        blank=True,
        on_delete=models.PROTECT,
        verbose_name='Transportista',
        help_text=
        '..creando un shipping con este transportista (si se deja vacío, el shipping se creará sin transportista)',
    )

    class Meta:
        unique_together = (
            'source_region',
            'resource',
        )

    def clean(self):
        # Evitamos movimientos entre inventarios de distinto tipo
        super().clean()
        if self.resource != self.target_inventory.resource:
            raise ValidationError({
                'resource':
                _('Inventory resource should be the same as resource.'),
                'target_inventory':
                _('Inventory resource should be the same as resource.'),
            })


class Tracking(BaseModel):
    STATUS_AWAITING = 0
    STATUS_SENT = 1
    STATUS_RECEIVED = 2
    STATUS_SCHEDULED = 3
    STATUS_CANCELLED = 4
    STATUS_IN_TRANSIT = 5

    STATUS_CHOICES = (
        (STATUS_AWAITING, 'Esperando'),
        (STATUS_SENT, 'Enviado'),
        (STATUS_RECEIVED, 'Recibido'),
        (STATUS_SCHEDULED, 'Programado'),
        (STATUS_CANCELLED, 'Cancelado'),
        (STATUS_IN_TRANSIT, 'En tránsito'),
    )

    status = models.IntegerField(
        default=STATUS_AWAITING,
        choices=STATUS_CHOICES,
        verbose_name='Estado',
    )
    scheduled_date = models.DateTimeField(
        db_index=True,
        null=True,
        blank=True,
        default=None,
        verbose_name='Programado',
    )
    record_date = models.DateTimeField(
        db_index=True,
        null=True,
        blank=True,
        default=timezone.now,
        verbose_name='Fecha de evento',
    )
    person = models.ForeignKey(
        'Person',
        null=True,
        related_name='tracking',
        blank=True,
        on_delete=models.PROTECT,
    )
    shipping = models.ForeignKey(
        'Shipping',
        null=False,
        related_name='tracking',
        blank=False,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name_plural = _('tracking')

    def __str__(self):

        try:
            date_string = self.record_date.strftime('%Y-%m-%d %H:%M')
        except AttributeError:
            date_string = 'DATE?'
        try:
            person_name = self.person.name
        except AttributeError:
            person_name = ''

        try:
            return f"{date_string} {person_name} -> {dict(self.STATUS_CHOICES).get(self.status)}"
        except IndexError:
            return 'UNKNOWN'

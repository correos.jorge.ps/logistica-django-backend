from django.utils.translation import gettext_lazy as _
from django.contrib.gis.db import models
from logistica.models.base import BaseModel


class Resource(BaseModel):
    TYPE_MAKER_MATERIAL = 0
    TYPE_PRODUCT_PART = 1
    TYPE_END_PRODUCT = 2

    TYPE_CHOICES = (
        (TYPE_MAKER_MATERIAL, 'Material para maker'),
        (TYPE_PRODUCT_PART, 'Parte de producto'),
        (TYPE_END_PRODUCT, 'Producto terminado'),
    )

    VOLUME_UNIT_LITER = 0
    VOLUME_UNIT_CHOICES = ((VOLUME_UNIT_LITER, 'Litro'), )

    resource_type = models.IntegerField(
        default=TYPE_MAKER_MATERIAL,
        choices=TYPE_CHOICES,
        verbose_name='Tipo de recurso',
    )
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name='Name',
    )
    sku = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name='SKU',
    )
    spl_link = models.URLField(
        null=True,
        blank=True,
        verbose_name='SPL link',
    )
    image_link = models.URLField(
        null=True,
        blank=True,
        verbose_name='Imagen link',
    )
    howto_link = models.URLField(
        null=True,
        blank=True,
        verbose_name='Instrucciones link',
    )
    volume = models.DecimalField(
        null=True,
        blank=True,
        max_digits=15,
        decimal_places=4,
        verbose_name='Volumen',
        help_text='Para productos con volúmen, como solución hidroalcohólica',
    )
    volume_unit = models.IntegerField(
        null=True,
        blank=True,
        default=None,
        choices=VOLUME_UNIT_CHOICES,
        verbose_name='Unidad del volúmen',
    )
    prevent_stats = models.BooleanField(
        default=False,
        verbose_name="Evitar que aparezca en las estadísticas",
    )

    class Meta:
        verbose_name_plural = _('resources')

    def __str__(self):
        return self.name

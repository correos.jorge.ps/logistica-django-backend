from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from django.contrib.gis.db import models
from logistica.models.base import BaseModel
from logistica.models.shipping import Shipping


class Inventory(BaseModel):
    name = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Name',
    )
    resource = models.ForeignKey(
        'Resource',
        null=False,
        related_name='inventories',
        blank=False,
        on_delete=models.PROTECT,
        verbose_name='Recurso',
    )
    owner = models.ForeignKey(
        'Person',
        null=False,
        related_name='inventories',
        blank=False,
        on_delete=models.PROTECT,
        verbose_name='Propietario (persona)',
    )
    quantity = models.IntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name='Cantidad original',
        help_text=
        'Representa la cantidad original del lote. Revisar cantidad disponible.'
    )
    track_batch = models.PositiveIntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name='Nº de lote original',
        help_text=
        'En caso de recibir este producto desde otro proveedor, indica el Nº de lote recibido'
    )
    photo_link = models.URLField(
        null=True,
        blank=True,
        verbose_name='Foto',
    )

    @property
    def movements_to_this(self):
        move_count = 0
        for shipping in self.shippings_in.filter(status__in=[
                Shipping.STATUS_SENT,
                Shipping.STATUS_RECEIVED,
                Shipping.STATUS_SCHEDULED,
                Shipping.STATUS_IN_TRANSIT,
        ]):
            move_count = move_count + shipping.quantity
        return move_count

    # Podría llamarse stock
    @property
    def quantity_available(self):
        consumed = 0
        for shipping in self.shippings.filter(status__in=[
                Shipping.STATUS_SENT,
                Shipping.STATUS_RECEIVED,
                Shipping.STATUS_SCHEDULED,
                Shipping.STATUS_IN_TRANSIT,
        ]):
            consumed = consumed + shipping.quantity
        # Cantidad fija indicada - cantidad de salida + cantidad de entrada
        return (self.quantity - consumed) + self.movements_to_this

    @property
    def auto_gen_batch(self):
        return self.pk

    @property
    def pad_batch(self, pad_size=10):
        return str(self.auto_gen_batch).zfill(pad_size)

    class Meta:
        verbose_name_plural = _('inventories')

    def __str__(self):
        return f"Disponible:{self.quantity_available} | {self.resource.name}"

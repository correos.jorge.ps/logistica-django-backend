# pylint: disable=too-many-locals,too-many-statements,too-many-lines
from django.apps import apps
from django.urls import reverse
from django.http import QueryDict
from django.conf import settings
from django.test import TestCase
from django.contrib.gis.geos import Point
from rest_framework.test import APITestCase

from logistica.models import (
    Region,
    Resource,
    Person,
    Request,
    Shipping,
    Inventory,
    RoleDisambiguation,
    get_global_config,
)


class PersonSignalTestCase(TestCase):
    count = 0

    def _create_user(self, username="usertest"):
        cls = apps.get_model(settings.AUTH_USER_MODEL)
        user = cls.objects.create(
            username=username, email=str(self.count) + 'asdf@example.com')
        self.count += 1
        return user

    def test_set_shpping_status_signal(self):
        region_not_allowed = Region.objects.create(
            name='a',
            default_person_allow_set_send_status=False,
        )
        region_allowed = Region.objects.create(
            name='a',
            default_person_allow_set_send_status=True,
        )

        maker1 = Person.objects.create(
            user=self._create_user('maker1'),
            role=Person.ROLE_PRODUCER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_PRODUCER, name='Maker'),
            region=region_not_allowed,
            point=Point(27, 27),
            cp='00',
        )
        self.assertFalse(maker1.can_set_send_status)

        maker2 = Person.objects.create(
            user=self._create_user('maker2'),
            role=Person.ROLE_PRODUCER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_PRODUCER, name='Maker'),
            region=region_allowed,
            point=Point(27, 27),
            cp='00',
        )
        self.assertTrue(maker2.can_set_send_status)

        region_not_allowed.default_person_allow_set_send_status = True
        region_not_allowed.save()
        maker1.save()
        self.assertTrue(maker1.can_set_send_status)

        region_not_allowed.default_person_allow_set_send_status = False
        region_not_allowed.save()
        maker1.save()
        self.assertFalse(maker1.can_set_send_status)


class BaseApiTestCase(APITestCase):
    lc_name = None
    rud_name = None
    count = 0

    def _post(self, data, kwargs=None):
        return self.client.post(
            reverse(self.lc_name, kwargs=kwargs),
            data,
            format='json',
        )

    def _create(self, data, kwargs=None):
        return self._post(data, kwargs)

    def _patch(self, data, kwargs=None):
        return self.client.patch(
            reverse(self.rud_name, kwargs=kwargs),
            data,
            format='json',
        )

    def _put(self, data, kwargs=None):
        return self.client.patch(
            reverse(self.rud_name, kwargs=kwargs),
            data,
            format='json',
        )

    def _read(self, kwargs=None):
        return self.client.get(
            reverse(self.rud_name, kwargs=kwargs),
            format='json',
        )

    def _delete(self, kwargs=None):
        return self.client.delete(
            reverse(self.rud_name, kwargs=kwargs),
            format='json',
        )

    def _list(self, filters=None, kwargs=None):
        if filters is None:
            return self.client.get(
                reverse(self.lc_name, kwargs=kwargs),
                format='json',
            )
        _q = QueryDict('', mutable=True)
        _q.update(filters)
        return self.client.get(
            '%s?%s' % (reverse(self.lc_name), _q.urlencode()),
            format='json',
        )

    def _create_user(self, username="usertest"):
        cls = apps.get_model(settings.AUTH_USER_MODEL)
        user = cls.objects.create(
            username=username,
            email=str(self.count) + 'adsf@example.com',
        )
        self.count += 1
        return user

    def get_one_region(self):
        region, _ = Region.objects.get_or_create(name="region test", )
        return region

    def get_one_resource(self, name='visera'):
        resource, _ = Resource.objects.get_or_create(name=name)
        return resource

    def get_one_maker(self, user):
        maker, _ = Person.objects.get_or_create(
            user=user,
            role=Person.ROLE_PRODUCER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_PRODUCER, name='Maker'),
            region=self.get_one_region(),
        )

        maker.point = Point(27, 27)
        maker.cp = '00'
        maker.save()
        return maker

    def get_one_consumer(self, user):
        consumer, _ = Person.objects.get_or_create(
            user=user,
            role=Person.ROLE_CONSUMER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_CONSUMER, name='Consumer'),
            region=self.get_one_region(),
        )

        consumer.point = Point(27, 27)
        consumer.cp = '00'
        consumer.save()
        return consumer

    def get_one_carrier(self, user):
        carrier, _ = Person.objects.get_or_create(
            user=user,
            role=Person.ROLE_CARRIER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_CARRIER, name='Carrier'),
            region=self.get_one_region(),
        )

        carrier.point = Point(27, 27)
        carrier.cp = '00'
        carrier.save()
        return carrier

    def login(self, user=None):
        if user is None:
            user = self._create_user()
        user.set_password('passtest')
        user.save()

        resp = self.client.post(
            reverse("jwt-create"),
            {
                'username': user.username,
                'password': 'passtest',
            },
            format='json',
        )
        token = resp.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return user, resp

    def logout(self):
        pass


class LogisticaTestCase(BaseApiTestCase):
    def test_person_create_list(self):
        user, _ = self.login()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_PRODUCER, name='producer')
        # Person creation
        resp = self.client.post(
            reverse("basic_person_list_create"),
            {
                'name': 'Person test',
                'role': Person.ROLE_PRODUCER,
                'role_disambiguation': role_disambiguation.pk,
                'cp': '00',
                'region': self.get_one_region().pk,
                'point': {
                    'type': 'Point',
                    'coordinates': [27, 27]
                },
            },
            format='json',
        )
        self.assertEqual(int(resp.data['user']), int(user.pk))
        person_pk = resp.data['pk']

        # person list
        resp = self.client.get(
            reverse("basic_person_list_create"),
            format='json',
        )
        self.assertEqual(resp.data.get('results')[0].get('pk'), person_pk)

        # person isolation (security)
        cls = apps.get_model(settings.AUTH_USER_MODEL)
        user = cls.objects.create(username="usertest2", email="asdf@asdf.com")
        self.login(user)
        resp = self.client.get(
            reverse("basic_person_list_create"),
            format='json',
        )
        self.assertEqual(resp.data['count'], 0)

    def test_person_inventory_list_create(self):
        user, _ = self.login()
        resource = self.get_one_resource()
        maker = self.get_one_maker(user)

        # Person creation
        resp = self.client.post(
            reverse(
                "basic_person_inventory_list_create",
                kwargs={'person_pk': maker.pk},
            ),
            {
                'resource': resource.pk,
                'quantity': 12,
                'region': self.get_one_region().pk,
            },
            format='json',
        )
        self.assertEqual(resp.data['owner'], maker.pk)
        self.assertEqual(resp.data['resource'], resource.pk)


class BasicPersonTestCase(BaseApiTestCase):
    lc_name = 'basic_person_list_create'
    rud_name = 'basic_person_retrieve_update_delete'

    def test_happy_creation_maker(self):
        """creando un person con todo lo que espera la api
        """
        user, _ = self.login()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_PRODUCER, name='producer')
        region = self.get_one_region()
        base_data = {
            'name': 'Person test',
            'role': Person.ROLE_PRODUCER,
            'role_disambiguation': role_disambiguation.pk,
            'cp': '00',
            'region': region.pk,
            'point': {
                'type': 'Point',
                'coordinates': [27, 27]
            },
        }
        resp = self._create(base_data)
        self.assertEqual(int(resp.data['user']), int(user.pk))
        self.assertEqual(int(resp.data['role']), Person.ROLE_PRODUCER)
        self.assertEqual(int(resp.data['region']), region.pk)
        self.assertEqual(int(resp.data['user']), int(user.pk))

    def test_bad_creation_maker(self):
        """Crear person sin indicar campos requeridos
        """
        self.login()
        region = self.get_one_region()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_PRODUCER, name='producer')
        base_data = {
            'name': 'Person test',
            'role': Person.ROLE_PRODUCER,
            'role_disambiguation': role_disambiguation.pk,
            'region': region.pk,
            'cp': '18014',
            'point': {
                'type': 'Point',
                'coordinates': [27, 27]
            },
        }
        # Creation
        bad_data = base_data.copy()
        del bad_data['name']
        resp = self._create(bad_data)
        self.assertEqual(resp.status_code, 400)

        # Role maker by default
        bad_data = base_data.copy()
        del bad_data['role']
        resp = self._create(bad_data)
        self.assertEqual(resp.data['role'], Person.ROLE_PRODUCER)

        bad_data = base_data.copy()
        del bad_data['region']
        resp = self._create(bad_data)
        self.assertEqual(resp.status_code, 400)

        bad_data = base_data.copy()
        del bad_data['cp']
        resp = self._create(bad_data)
        self.assertEqual(resp.status_code, 400)

    def test_list_persons(self):
        """crear varios persons con varios roles, listarlos y aplicar filtro
        de role
        """
        self.login()
        region = self.get_one_region()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_PRODUCER, name='producer')
        base_data = {
            'name': 'Person test',
            'role': Person.ROLE_PRODUCER,
            'role_disambiguation': role_disambiguation.pk,
            'cp': '00',
            'region': region.pk,
            'point': {
                'type': 'Point',
                'coordinates': [27, 27]
            },
        }
        resp = self._create(base_data)
        self.assertEqual(resp.status_code, 201)

        base_data.update({'role': Person.ROLE_CONSUMER})
        resp = self._create(base_data)
        self.assertEqual(resp.status_code, 201)

        base_data.update({'role': Person.ROLE_LOGISTICS})
        resp = self._create(base_data)
        self.assertEqual(resp.status_code, 201)

        base_data.update({'role': Person.ROLE_CARRIER})
        resp = self._create(base_data)
        self.assertEqual(resp.status_code, 201)

        resp = self._list()
        self.assertEqual(resp.data.get('count'), 4)

        resp = self._list(filters={'role': Person.ROLE_PRODUCER})
        self.assertEqual(resp.data.get('count'), 1)

        resp = self._list(filters={'role': Person.ROLE_CONSUMER})
        self.assertEqual(resp.data.get('count'), 1)

        resp = self._list(filters={'role': Person.ROLE_LOGISTICS})
        self.assertEqual(resp.data.get('count'), 1)

        resp = self._list(filters={'role': Person.ROLE_CARRIER})
        self.assertEqual(resp.data.get('count'), 1)

    def test_bad_role_update(self):
        """Crear un person con role y comprobar que el role no se puede
        cambiar para evitar escalada de privilegios
        """
        self.login()
        region = self.get_one_region()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_PRODUCER, name='producer')
        base_data = {
            'name': 'Person test',
            'role': Person.ROLE_PRODUCER,
            'role_disambiguation': role_disambiguation.pk,
            'cp': '00',
            'region': region.pk,
            'point': {
                'type': 'Point',
                'coordinates': [27, 27]
            },
        }
        resp = self._create(base_data)
        self.assertEqual(resp.status_code, 201)
        person_pk = resp.data.get('pk')

        resp = self._patch(
            {
                'role': Person.ROLE_LOGISTICS
            },
            kwargs={'person_pk': person_pk},
        )
        self.assertEqual(resp.data.get('role'), Person.ROLE_PRODUCER)

        new_data = base_data.copy()
        new_data.update({'role': Person.ROLE_LOGISTICS})
        resp = self._put(
            new_data,
            kwargs={'person_pk': person_pk},
        )

        self.assertEqual(resp.data.get('role'), Person.ROLE_PRODUCER)


class BasicConsumerRequestTestCase(BaseApiTestCase):
    lc_name = 'basic_person_request_list_create'
    rud_name = 'basic_person_request_ru'

    def test_create_request(self):
        consumer_user = self._create_user('hospital')
        consumer = self.get_one_consumer(consumer_user)
        maker_user = self._create_user('maker')
        maker = self.get_one_maker(maker_user)
        resource = self.get_one_resource()
        region = self.get_one_region()
        consumer.region = region
        consumer.save()
        maker.region = region
        maker.save()
        inventory = Inventory.objects.create(
            owner=maker,
            resource=resource,
            quantity=123,
        )
        self.login(consumer_user)

        resp = self._create(
            {
                'inventory': inventory.pk,
                'resource': resource.pk,
                'quantity': 123,
            },
            kwargs={'person_pk': consumer.pk},
        )
        request_pk = resp.data.get('pk')
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(resp.data.get('consumer'), consumer.pk)
        self.assertEqual(
            resp.data.get('resource_object').get('pk'),
            resource.pk,
        )
        self.assertEqual(
            resp.data.get('consumer_object').get('pk'),
            consumer.pk,
        )

        resp = self._patch(
            {
                'status': Request.STATUS_CANCELLED
            },
            kwargs={
                'person_pk': consumer.pk,
                'request_pk': request_pk,
            },
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data.get('status'), Request.STATUS_CANCELLED)


class BasicMakerPersonInventoryTestCase(BaseApiTestCase):
    lc_name = 'basic_person_inventory_list_create'
    rud_name = 'basic_person_inventory_retrieve_update_delete'

    def test_happy_inventory_create(self):
        user, _ = self.login()
        maker = self.get_one_maker(user)
        resource = self.get_one_resource()
        base_data = {
            "resource": resource.pk,
            "quantity": 123,
            "track_batch": 0,
        }

        resp = self._create(base_data, kwargs={'person_pk': maker.pk})
        self.assertEqual(resp.data.get('owner'), maker.pk)
        self.assertEqual(resp.data.get('resource'), resource.pk)
        self.assertEqual(resp.data.get('resource'), resource.pk)
        self.assertEqual(resp.data.get('quantity'), 123)
        self.assertIsNotNone(resp.data.get('pad_batch'))

    def test_shipping_status_of_inventory_update(self):
        user, _ = self.login()
        maker = self.get_one_maker(user)
        carrier = self.get_one_carrier(self._create_user('carrier'))
        consumer = self.get_one_consumer(self._create_user('consumer'))
        resource = self.get_one_resource()
        base_data = {
            "resource": resource.pk,
            "quantity": 123,
            "track_batch": 0,
        }

        resp = self._create(base_data, kwargs={'person_pk': maker.pk})
        inventory = Inventory.objects.get(pk=resp.data.get('pk'))
        shipping = Shipping.objects.create(
            inventory=inventory,
            request=Request.objects.create(
                consumer=consumer,
                resource=resource,
            ),
            carrier=carrier,
        )
        # No se permite modificar ningún estado
        resp = self.client.patch(
            reverse(
                'basic_person_shipping_update',
                kwargs={
                    'person_pk': maker.pk,
                    'shipping_pk': shipping.pk
                }),
            {'status': Shipping.STATUS_CANCELLED},
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

        # No se permite modificar ningún estado
        resp = self.client.patch(
            reverse(
                'basic_person_shipping_update',
                kwargs={
                    'person_pk': maker.pk,
                    'shipping_pk': shipping.pk
                }),
            {'status': Shipping.STATUS_SENT},
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

        # Permitir modificar el estado send seteando
        maker.person_allow_set_send_status = True
        maker.save()

        # No se permite modificar ae stado cancelled
        resp = self.client.patch(
            reverse(
                'basic_person_shipping_update',
                kwargs={
                    'person_pk': maker.pk,
                    'shipping_pk': shipping.pk
                }),
            {'status': Shipping.STATUS_CANCELLED},
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

        # Se permite modificar estado sent
        resp = self.client.patch(
            reverse(
                'basic_person_shipping_update',
                kwargs={
                    'person_pk': maker.pk,
                    'shipping_pk': shipping.pk
                }),
            {'status': Shipping.STATUS_SENT},
            format='json',
        )
        self.assertEqual(resp.status_code, 200)

        # Revocamos permisos al person
        maker.person_allow_set_send_status = None
        maker.save()

        # Damos permisos a la región
        maker.region.default_person_allow_set_send_status = True
        maker.region.save()
        # No se permite modificar ae stado cancelled
        resp = self.client.patch(
            reverse(
                'basic_person_shipping_update',
                kwargs={
                    'person_pk': maker.pk,
                    'shipping_pk': shipping.pk
                }),
            {'status': Shipping.STATUS_CANCELLED},
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

        # Se permite modificar estado sent
        resp = self.client.patch(
            reverse(
                'basic_person_shipping_update',
                kwargs={
                    'person_pk': maker.pk,
                    'shipping_pk': shipping.pk
                }),
            {'status': Shipping.STATUS_SENT},
            format='json',
        )
        self.assertEqual(resp.status_code, 200)

    def test_happy_inventory_list(self):
        user, _ = self.login()
        maker = self.get_one_maker(user)
        resource = self.get_one_resource()
        base_data = {
            "resource": resource.pk,
            "quantity": 123,
            "track_batch": 0,
        }

        self._create(base_data, kwargs={'person_pk': maker.pk})
        self._create(base_data, kwargs={'person_pk': maker.pk})
        self._create(base_data, kwargs={'person_pk': maker.pk})
        self._create(base_data, kwargs={'person_pk': maker.pk})
        self._create(base_data, kwargs={'person_pk': maker.pk})

        resp = self._list(kwargs={'person_pk': maker.pk})
        self.assertEqual(resp.data.get('count'), 5)

    def test_inventory_create_day_limit(self):
        user, _ = self.login()
        maker = self.get_one_maker(user)
        resource = self.get_one_resource()
        base_data = {
            "resource": resource.pk,
            "quantity": 123,
            "track_batch": 0,
        }

        conf = get_global_config()
        for _i in range(0, conf.day_inventory_limit + 1):
            resp = self._create(base_data, kwargs={'person_pk': maker.pk})
            self.assertEqual(resp.status_code, 201)

        resp = self._create(base_data, kwargs={'person_pk': maker.pk})
        self.assertEqual(resp.status_code, 400)

    def test_inventory_rud(self):
        user, _ = self.login()
        maker = self.get_one_maker(user)
        resource = self.get_one_resource()
        base_data = {
            "resource": resource.pk,
            "quantity": 123,
            "track_batch": 0,
        }
        resp = self._create(base_data, kwargs={'person_pk': maker.pk})
        self.assertEqual(resp.data.get('quantity'), 123)
        inventory_pk = resp.data.get('pk')

        resp = self._patch(
            {
                'quantity': 1
            },
            kwargs={
                'person_pk': maker.pk,
                'inventory_pk': inventory_pk
            },
        )
        self.assertEqual(resp.data.get('quantity'), 1)

        new_data = base_data.copy()
        new_data['quantity'] = 2
        resp = self._put(
            new_data,
            kwargs={
                'person_pk': maker.pk,
                'inventory_pk': inventory_pk
            },
        )
        self.assertEqual(resp.data.get('quantity'), 2)

        resp = self._read(kwargs={
            'person_pk': maker.pk,
            'inventory_pk': inventory_pk
        })
        self.assertEqual(resp.status_code, 200)

        resp = self._delete(kwargs={
            'person_pk': maker.pk,
            'inventory_pk': inventory_pk
        })
        self.assertEqual(resp.status_code, 204)  # deleted

    def test_denied_rud_inventory_due_already_shipped(self):
        user, _ = self.login()
        maker = self.get_one_maker(user)
        resource = self.get_one_resource()
        base_data = {
            "resource": resource.pk,
            "quantity": 123,
            "track_batch": 0,
        }
        resp = self._create(base_data, kwargs={'person_pk': maker.pk})

        consumer_user = self._create_user('hospital')
        inventory = Inventory.objects.get(pk=resp.data.get('pk'))
        request = Request.objects.create(
            consumer=self.get_one_consumer(consumer_user),
            resource=resource,
            quantity=12,
        )
        Shipping.objects.create(
            status=Shipping.STATUS_SENT,
            request=request,
            inventory=inventory,
            quantity=123,
        )

        resp = self._patch(
            {
                'quantity': 1
            },
            kwargs={
                'person_pk': maker.pk,
                'inventory_pk': inventory.pk,
            },
        )
        self.assertEqual(resp.status_code, 403)  # denied patch

        new_data = base_data.copy()
        new_data['quantity'] = 3
        resp = self._put(
            new_data,
            kwargs={
                'person_pk': maker.pk,
                'inventory_pk': inventory.pk,
            },
        )
        self.assertEqual(resp.status_code, 403)  # denied put

        resp = self._delete(kwargs={
            'person_pk': maker.pk,
            'inventory_pk': inventory.pk
        })
        self.assertEqual(resp.status_code, 403)  # denied deletion

        # still can read
        resp = self._read(kwargs={
            'person_pk': maker.pk,
            'inventory_pk': inventory.pk
        })
        self.assertEqual(resp.status_code, 200)

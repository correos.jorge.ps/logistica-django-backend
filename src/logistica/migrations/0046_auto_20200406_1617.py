# Generated by Django 2.2.11 on 2020-04-06 16:17

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logistica', '0045_auto_20200406_1614'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='alta_db',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='region',
            name='cod_postal',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='region',
            name='codigo_ine',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='region',
            name='geom',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=4326),
        ),
        migrations.AddField(
            model_name='region',
            name='id_cp',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]

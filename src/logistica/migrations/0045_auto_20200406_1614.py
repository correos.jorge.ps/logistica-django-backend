# Generated by Django 2.2.11 on 2020-04-06 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logistica', '0044_auto_20200405_2050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='name',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Región'),
        ),
    ]

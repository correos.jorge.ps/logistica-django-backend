# Generated by Django 2.2.11 on 2020-04-07 14:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logistica', '0052_remove_uuid_null'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='ine_code',
            field=models.IntegerField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='region',
            name='postal_code',
            field=models.CharField(blank=True, db_index=True, max_length=80, null=True),
        ),
    ]

"""Serializers relativos a los makers.
"""
from django.utils import timezone
from rest_framework import serializers
from logistica.models import (
    RoleDisambiguation,
    Person,
    Inventory,
    Resource,
    Region,
    Shipping,
    Request,
    get_global_config,
)


class BasicRoleDisambiguationSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoleDisambiguation
        fields = (
            'pk',
            'role',
            'name',
        )
        read_only_fields = fields


class BasicResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = (
            'pk',
            'name',
            'sku',
            'resource_type',
            'spl_link',
            'image_link',
            'howto_link',
        )
        read_only_fields = (
            'pk',
            'name',
            'sku',
            'resource_type',
            'spl_link',
            'image_link',
            'howto_link',
        )


class BasicRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = (
            'pk',
            'name',
            'postal_code',
            'level',
        )
        read_only_fields = fields


class BasicReadOnlyRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = (
            'pk',
            'name',
            'postal_code',
            'level',
        )
        read_only_fields = fields


class BasicInventoryShippingConsumerSerializer(serializers.ModelSerializer):
    # region_object = BasicReadOnlyRegionSerializer(
    #     read_only=True,
    #     source='region',
    # )

    class Meta:
        model = Person
        fields = (
            'pk',
            'role',
        )
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class BasicInventoryShippingCarrierSerializer(serializers.ModelSerializer):
    # region_object = BasicReadOnlyRegionSerializer(
    #     read_only=True,
    #     source='region',
    # )

    class Meta:
        model = Person
        fields = (
            'pk',
            'role',
            'name',
        )
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class BasicRequestShippingSerializer(serializers.ModelSerializer):
    carrier_object = BasicInventoryShippingCarrierSerializer(
        read_only=True,
        source='carrier',
    )

    class Meta:
        model = Shipping
        fields = (
            'pk',
            'uuid',
            'status',
            'carrier',
            'carrier_object',
            'created_date',
            'last_updated_date',
            'quantity',
        )
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class BasicRequestSerializer(serializers.ModelSerializer):
    consumer_object = BasicInventoryShippingConsumerSerializer(
        read_only=True,
        source='consumer',
    )
    shippings_object = BasicRequestShippingSerializer(
        many=True,
        read_only=True,
        source='shippings',
    )
    resource_object = BasicResourceSerializer(
        read_only=True,
        source='resource',
    )

    class Meta:
        model = Request
        fields = (
            'pk',
            'consumer',
            'status',
            'consumer_object',
            'resource',
            'resource_object',
            'shippings',
            'shippings_object',
            'quantity',
            'done',
            'created_date',
            'last_updated_date',
        )
        read_only_fields = (
            'pk',
            'consumer',
            'consumer_object',
            'resource_object',
            'shippings',
            'shippings_object',
            'done',
            'created_date',
            'last_updated_date',
        )

    def validate(self, attrs):
        data = super().validate(attrs)
        person_pk = self.context.get('view').kwargs.get('person_pk')
        user = self.context.get('request').user
        try:
            person = Person.objects.get(
                pk=person_pk,
                user=user,
            )
        except Person.DoesNotExist:
            raise serializers.ValidationError(
                {
                    'consumer': 'Permision denied'
                },
                code='denied',
            )
        # Permitimos crear pedidos sólo a makers y a centros de salud
        if person.role not in (Person.ROLE_PRODUCER, Person.ROLE_CONSUMER):
            raise serializers.ValidationError(
                {
                    'consumer.role': 'Permision denied'
                },
                code='denied',
            )
        data['consumer'] = person
        return data


class BasicShippingSerializer(serializers.ModelSerializer):
    carrier_object = BasicInventoryShippingCarrierSerializer(
        read_only=True,
        source='carrier',
    )
    request_object = BasicRequestSerializer(read_only=True, source='request')

    class Meta:
        model = Shipping
        fields = (
            'pk',
            'uuid',
            'status',
            'carrier',
            'carrier_object',
            'request',
            'request_object',
            'quantity',
        )
        read_only_fields = (
            'pk',
            'uuid',
            # 'status', # Permitimos setear status con algunas condiciones
            'carrier',
            'carrier_object',
            'request',
            'request_object',
            'quantity',
        )

    def create(self, validated_data):
        return None


# WARNING: Este serializer sólo puede utilizarse para
# mostrarle datos al dueño de los datos (LOPD)
class BasicPersonSerializer(serializers.ModelSerializer):
    region_object = BasicReadOnlyRegionSerializer(
        read_only=True,
        source='region',
    )

    class Meta:
        model = Person
        # WARNING: Si añades fields aquí, recuerda que en la admin
        # los fields están puestos a mano
        fields = (
            'pk',
            'role',
            'name',
            'contact_person_name',
            'region',
            'region_object',
            'telegram_nick',
            'telegram_id',
            'address',
            'number',
            'city',
            'province',
            'cp',
            'country',
            'phone',
            'point',
            'production_capacity',
            'machine_models',
            'machine_count',
            'logistics_need',
            'user',
            'can_set_send_status',
            'can_set_received_status',
            'role_disambiguation',
        )
        read_only_fields = (
            'pk',
            'user',
            'region_object',
            'can_set_send_status',
            'can_set_received_status',
        )

    def create(self, validated_data):
        validated_data.update({
            # 'role': Person.ROLE_PRODUCER,
            'user': self.context.get('request').user,
        })
        return super().create(validated_data)

    def update(self, instance, validated_data):
        # Security check. Un role no puede cambiarse para evitar
        # escalada de privilegios
        if 'role' in validated_data:
            del validated_data['role']
        return super().update(instance, validated_data)


class BasicInventorySerializer(serializers.ModelSerializer):
    quantity_available = serializers.IntegerField(read_only=True)
    auto_gen_batch = serializers.IntegerField(read_only=True)
    pad_batch = serializers.CharField(read_only=True)
    shippings_object = BasicShippingSerializer(
        read_only=True,
        many=True,
        source='shippings',
    )
    resource_object = BasicResourceSerializer(
        read_only=True,
        source='resource',
    )

    class Meta:
        model = Inventory
        fields = (
            'pk',
            'resource',
            'resource_object',
            'owner',
            'shippings_object',
            'quantity',
            'track_batch',
            'photo_link',
            'auto_gen_batch',
            'pad_batch',
            'quantity_available',
            'created_date',
            'last_updated_date',
        )
        read_only_fields = (
            'pk',
            'resource_object',
            'owner',
            'shippings_object',
            'auto_gen_batch',
            'pad_batch',
            'quantity_available',
            'created_date',
            'last_updated_date',
        )

    def validate(self, attrs):
        data = super().validate(attrs)
        conf = get_global_config()
        _delta = timezone.now() - timezone.timedelta(hours=24)
        queryset = Inventory.objects.filter(
            owner__user=self.context.get('request').user,
            created_date__gte=_delta,
        )
        if queryset.count() > conf.day_inventory_limit:
            raise serializers.ValidationError(
                {
                    'quantity': 'Limit reached'
                },
                code='denied',
            )
        person_pk = self.context.get('view').kwargs.get('person_pk')
        user = self.context.get('request').user
        try:
            person = Person.objects.get(user=user, pk=person_pk)
        except Person.DoesNotExist:
            raise serializers.ValidationError(
                {
                    'owner': 'Permission denied'
                },
                code='denied',
            )
        # permitimos crear inventories sólo a persons de tipo maker o
        # almacén
        if person.role not in (Person.ROLE_PRODUCER, ):
            raise serializers.ValidationError(
                {
                    'owner.role': 'Permission denied'
                },
                code='denied',
            )
        data['owner'] = person
        return data

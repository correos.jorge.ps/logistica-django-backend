from django.core.management.commands.makemessages import Command as MakeMessagesCommand
from django.conf import settings
from django.utils import translation


class Command(MakeMessagesCommand):
    help = 'Generate .po messages for all configured langs'

    def handle(self, *args, **options):
        for lang in settings.LANGUAGES:
            options['locale'].append(translation.to_locale(lang[0]))

        super().handle(*args, **options)
        self.stdout.write('Archivos .po generados. Ya puedes editarlos.')
        self.stdout.write(
            'Next step after .po edit: python manage.py compilemessages')

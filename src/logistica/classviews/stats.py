import operator
from functools import reduce
import pytz
from django.db.models import Q
from django.db.models import Sum
from django.utils import timezone
from rest_framework import generics
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import FilterSet, filters, DjangoFilterBackend
import django_filters
from logistica import models
from logistica.serializers import stats as serializers
from logistica.filters import SumDistinctHACK


class NumberInFilter(django_filters.BaseInFilter, django_filters.NumberFilter):
    pass


class StatsBuildResourceL(generics.ListAPIView):
    """
    get: Listado de recursos construidos con campo de suma por piezas
    """

    class StatsResourceFilter(FilterSet):
        inventory_created_date = filters.BaseRangeFilter(
            method='filter_inventory_created_date',
            help_text=
            'Format: 2020-01-01-00:00:00,2020-12-31-00:00:00. (UTC) "now" is also accepted',
            required=True,
        )
        inventories__owner__region__in = NumberInFilter(
            field_name='inventories__owner__region__pk',
            lookup_expr='in',
            help_text='1,2,3,4',
        )

        inventories__owner__region__gt1level = django_filters.CharFilter(
            method='filter_inventories_owner__region__gt1level',
            help_text='Filtra recursivamente por regiones de nivel mayor a 1',
        )

        def filter_inventories_owner__region__gt1level(self, queryset, _name,
                                                       value):
            def q_one_region_pk(region_pk):
                try:
                    region = models.Region.objects.get(pk=int(region_pk))
                except models.Region.DoesNotExist:
                    return None
                if region.level == 1:
                    return None
                uplevels = region.level - 1
                query_key = 'inventories__owner__region' + (
                    '__parent' * uplevels)
                return Q(**{query_key: region})

            values = value.split(',')
            if not values:
                return queryset

            q_queries = list()
            for region_pk in values:
                q_query = q_one_region_pk(region_pk.strip())
                if q_query is not None:
                    q_queries.append(q_query)
            if q_queries:
                return queryset.filter(Q(reduce(operator.or_, q_queries)))
            return queryset

        def filter_inventory_created_date(self, queryset, _name, value):
            if value[0] == 'now':
                start_date = timezone.now()
            else:
                start_date = timezone.datetime.strptime(
                    value[0],
                    '%Y-%m-%d-%H:%M:%S',
                )
                start_date = pytz.utc.localize(start_date)
            if value[1] == 'now':
                end_date = timezone.now()
            else:
                end_date = timezone.datetime.strptime(
                    value[1],
                    '%Y-%m-%d-%H:%M:%S',
                )
                end_date = pytz.utc.localize(end_date)

            return queryset.annotate(
                total=Sum(
                    'inventories__quantity',
                    filter=Q(
                        inventories__created_date__gte=start_date,
                        inventories__created_date__lte=end_date),
                ))

        class Meta:
            model = models.Resource
            fields = (
                'inventory_created_date',
                'inventories__owner__region',
                'inventories__owner__region__in',
                'inventories__owner__region__gt1level',
            )

    serializer_class = serializers.StatsResourceSerializer
    permission_classes = (AllowAny, )
    queryset = models.Resource.objects.filter()
    filterset_class = StatsResourceFilter
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('resource_type', )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Resource.objects.none()
        return models.Resource.objects.all()


class StatsConsumedResourceL(generics.ListAPIView):
    """
    get: Listado de recursos con campo de suma por piezas
    """

    class StatsResourceFilter(FilterSet):
        inventory_created_date = filters.BaseRangeFilter(
            method='filter_inventory_created_date',
            help_text=
            'Format: 2020-01-01-00:00:00,2020-12-31-00:00:00. (UTC) "now" is also accepted',
            required=True,
        )

        def filter_inventory_created_date(self, queryset, _name, value):
            if value[0] == 'now':
                start_date = timezone.now()
            else:
                start_date = timezone.datetime.strptime(
                    value[0],
                    '%Y-%m-%d-%H:%M:%S',
                )
                start_date = pytz.utc.localize(start_date)
            if value[1] == 'now':
                end_date = timezone.now()
            else:
                end_date = timezone.datetime.strptime(
                    value[1],
                    '%Y-%m-%d-%H:%M:%S',
                )
                end_date = pytz.utc.localize(end_date)

            return queryset.annotate(
                total=SumDistinctHACK(
                    'inventories__shippings__quantity',
                    distinct=True,
                    filter=Q(
                        ~Q(inventories__shippings__status=models.Shipping.
                           STATUS_CANCELLED)
                        &
                        Q(inventories__shippings__created_date__gte=start_date,
                          inventories__shippings__created_date__lte=end_date,
                          inventories__shippings__request__status=models.
                          Request.STATUS_RECEIVED), ),
                ))

    serializer_class = serializers.StatsResourceSerializer
    permission_classes = (AllowAny, )
    queryset = models.Resource.objects.filter()
    filterset_class = StatsResourceFilter
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('resource_type', )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Resource.objects.none()
        return models.Resource.objects.all()


class StatsByDayResourcesL(generics.ListAPIView):
    """
    get: estadísticas por día y totales
    """

    class StatsByDayFilter(FilterSet):
        from_date = filters.DateFilter(
            method='filter_from_date',
            required=True,
        )
        region = filters.NumberFilter(
            method='filter_region',
            required=False,
        )

        def filter_from_date(self, queryset, _name, _value):
            # El valor del filtro será usado en el serializer
            return queryset

        def filter_region(self, queryset, _name, _value):
            # El valor del filtro será usado en el serializer
            return queryset

    serializer_class = serializers.StatsByDayResourcesSerializer
    permission_classes = (AllowAny, )
    queryset = models.Resource.objects.filter(prevent_stats=False)
    filterset_class = StatsByDayFilter
    filter_backends = (DjangoFilterBackend, )

    def get_serializer_context(self):
        if getattr(self, 'swagger_fake_view', False):
            return None
        context = super().get_serializer_context()

        date_value = self.request.query_params.get('from_date')
        from_date = timezone.datetime.strptime(
            date_value,
            '%Y-%m-%d',
        )
        from_date = pytz.utc.localize(from_date)
        stats_queryset = models.StatsByDay.objects.filter(date__gte=from_date)
        region_pk = self.request.query_params.get('region', None)
        if region_pk:
            stats_queryset = stats_queryset.filter(region__pk=region_pk)
        context.update({
            "stats_queryset": stats_queryset,
        })
        return context


class StatsByDaySumResourcesL(generics.ListAPIView):
    """
    get: estadísticas por día y totales
    """

    # class StatsByDayFilter(FilterSet):
    #     from_date = filters.DateFilter(
    #         method='filter_from_date',
    #         required=False,
    #     )
    #     region = filters.NumberFilter(
    #         method='filter_region',
    #         required=False,
    #     )

    #     def filter_from_date(self, queryset, _name, _value):
    #         # El valor del filtro será usado en el serializer
    #         return queryset

    #     def filter_region(self, queryset, _name, _value):
    #         # El valor del filtro será usado en el serializer
    #         return queryset

    serializer_class = serializers.StatsByDaySumResourcesSerializer
    permission_classes = (AllowAny, )
    queryset = models.Resource.objects.filter(prevent_stats=False).annotate(
        build_static_value=Sum('stats_by_day__build_static_value'),
        build_calculated_value=Sum('stats_by_day__build_calculated_value'),
        deliver_static_value=Sum('stats_by_day__deliver_static_value'),
        deliver_calculated_value=Sum('stats_by_day__deliver_calculated_value'),
    )
    # filterset_class = StatsByDayFilter
    filter_backends = (DjangoFilterBackend, )

from django.apps import apps
from django.urls import reverse
from django.conf import settings
from django.http import QueryDict
from django.contrib.gis.geos import Point
from rest_framework.test import APITestCase
from logistica.models import (
    Region,
    Resource,
    Person,
    Request,
    Inventory,
    RoleDisambiguation,
    Shipping,
)


class BaseApiTestCase(APITestCase):
    count = 0

    def _create_new_user(self, username="usertest"):
        cls = apps.get_model(settings.AUTH_USER_MODEL)
        user = cls.objects.create(
            username=username, email=str(self.count) + 'asdf@example.com')
        self.count += 1
        return user

    def get_or_create_one_region(self):
        region, _ = Region.objects.get_or_create(name="region test", )
        return region

    def get_or_create_one_resource(self):
        resource, _ = Resource.objects.get_or_create(name="visera")
        return resource

    def get_new_logistica_person(self, user):
        logistica_person = Person.objects.create(
            user=user,
            role=Person.ROLE_LOGISTICS,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_LOGISTICS, name='Logistics'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return logistica_person

    def get_new_consumer_person(self, username="consumer"):
        consumer_person = Person.objects.create(
            user=self._create_new_user(username),
            role=Person.ROLE_CONSUMER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_CONSUMER, name='Consumer'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return consumer_person

    def get_new_maker_person(self, username="maker"):
        maker_person = Person.objects.create(
            user=self._create_new_user(username),
            role=Person.ROLE_PRODUCER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_PRODUCER, name='Maker'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return maker_person

    def get_midpoint_person(self, username='midpoint'):
        maker_person = Person.objects.create(
            user=self._create_new_user(username),
            role=Person.ROLE_MIDPOINT,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_MIDPOINT, name='Midpoint'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return maker_person

    def get_new_carrier_person(self):
        carrier_person = Person.objects.create(
            user=self._create_new_user('carrier'),
            role=Person.ROLE_CARRIER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_CARRIER, name='Carrier'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return carrier_person

    def _post(self, urlname=None, data=None, kwargs=None):
        return self.client.post(
            reverse(urlname, kwargs=kwargs),
            data,
            format='json',
        )

    def _patch(self, urlname=None, data=None, kwargs=None):
        return self.client.patch(
            reverse(urlname, kwargs=kwargs),
            data,
            format='json',
        )

    def _create(self, urlname=None, data=None, kwargs=None):
        return self._post(urlname, data, kwargs)

    def _list(self, urlname=None, filters=None, kwargs=None):
        if filters is None:
            return self.client.get(
                reverse(urlname, kwargs=kwargs),
                format='json',
            )
        _q = QueryDict('', mutable=True)
        _q.update(filters)
        return self.client.get(
            '%s?%s' % (reverse(urlname, kwargs=kwargs), _q.urlencode()),
            format='json',
        )

    def _read(self, urlname=None, kwargs=None):
        return self.client.get(
            reverse(urlname, kwargs=kwargs),
            format='json',
        )

    def login(self, user=None):
        if user is None:
            user = self._create_new_user()
        user.set_password('passtest')
        user.save()

        resp = self.client.post(
            reverse("jwt-create"),
            {
                'username': user.username,
                'password': 'passtest',
            },
            format='json',
        )
        token = resp.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return user, resp

    def logout(self):
        pass


class ExportTestCase(BaseApiTestCase):
    def test_shipping_perms(self):
        user, _ = self.login()
        # role_disambiguation = RoleDisambiguation.objects.create(
        #     role=RoleDisambiguation.ROLE_MIDPOINT, name='midpoint')
        region1 = self.get_or_create_one_region()
        region2 = Region.objects.create(name="region test2")
        Region.objects.create(name="region test2")

        maker1 = self.get_new_maker_person()
        maker1.region = region1
        maker1.save()
        maker2 = self.get_new_maker_person("maker2")
        maker2.region = region2
        maker2.save()
        consumer1 = self.get_new_consumer_person('consumer1')
        consumer1.region = region1
        consumer1.save()
        resource = self.get_or_create_one_resource()
        Request.objects.create(
            resource=resource,
            consumer=consumer1,
            quantity=100,
        )

        logistics_person = self.get_new_logistica_person(user)

        maker1_inventory = Inventory.objects.create(
            resource=resource,
            owner=maker1,
            quantity=100,
        )
        maker2_inventory = Inventory.objects.create(
            resource=resource,
            owner=maker2,
            quantity=100,
        )
        Shipping.objects.create(
            quantity=80,
            inventory=maker1_inventory,
            move=maker2_inventory,
        )

        logistics_person.logistica_region_write.add(region1)
        logistics_person.logistica_region_write.add(region2)

        self._list(
            urlname='middle_person_shipping_lc',
            filters={
                'export': 0,
            },
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )

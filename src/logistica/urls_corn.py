from django.conf.urls import url
from .classviews import corn as cornviews

urlpatterns = [
    # Endpoints destinados al transportista
    # Shipping
    url(
        r'^person/(?P<person_pk>\d+)/shipping/$',
        cornviews.CornShippingL.as_view(),
        name='corn_person_shipping_l',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/shipping/(?P<shipping_pk>\d+)/$',
        cornviews.CornShippingRU.as_view(),
        name='corn_person_shipping_ru',
    ),
]

import uuid as uuid_lib
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.core import validators
from django.utils.deconstruct import deconstructible
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import CIEmailField, CICharField
from django.core.exceptions import ValidationError


def username_bad_words_validator(value):
    if 'admin' in value.lower():
        raise ValidationError(
            _('%(value)s is not a valid username'),
            params={'value': value},
        )
    if 'superuser' in value.lower():
        raise ValidationError(
            _('%(value)s is not a valid username'),
            params={'value': value},
        )
    if 'coronavirus' in value.lower():
        raise ValidationError(
            _('%(value)s is not a valid username'),
            params={'value': value},
        )

    if 'coronavirusmakers' in value.lower():
        raise ValidationError(
            _('%(value)s is not a valid username'),
            params={'value': value},
        )

    if 'coronavirusmakersadmin' in value.lower():
        raise ValidationError(
            _('%(value)s is not a valid username'),
            params={'value': value},
        )


@deconstructible
class ValidCharsUsernameValidator(validators.RegexValidator):
    regex = r'^[\w.+-]+$'
    message = _('Enter a valid username. This value may contain only letters, '
                'numbers, and ./+/-/_ characters.')
    flags = 0


class User(AbstractUser):
    """Custom user model
    """
    uuid = models.UUIDField(
        db_index=True,
        default=uuid_lib.uuid4,
        editable=False,
    )
    username_validator = ValidCharsUsernameValidator()
    username = CICharField(
        'username',
        max_length=150,
        unique=True,
        help_text=
        'Required. 150 characters or fewer. Letters, digits and ./+/-/_ only.',
        validators=[username_validator, username_bad_words_validator],
        error_messages={
            'unique': "A user with that username already exists.",
        },
    )
    email = CIEmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

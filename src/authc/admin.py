from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as OrigUserAdmin
from authc.models import User


@admin.register(User)
class UserAdmin(OrigUserAdmin):
    pass
